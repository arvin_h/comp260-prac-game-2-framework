﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {
    public float speed = 20f;
    public float force = 10f;
    public Vector2 move;
    public Vector2 velocity;
    public float MaxSpeed = 5.0f;
    public float acceleration = 1.0f;
    public float brake = 5.0f;
    public float turnSpeed = 30.0f;
    public float Player;
    public float destroyRadius = 1.0f;



    private Rigidbody rigidbody;

    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }


    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;

    }


	
	// Update is called once per frame
	void Update () {
        Debug.Log("Time = " +Time.time);
     
        float turn;

        float forwards;

        if (this.gameObject.tag == "Player")
        {

            turn = Input.GetAxis("H1");
            forwards = Input.GetAxis("V1");
        }
        else
        {
            turn = Input.GetAxis("H2");
            forwards = Input.GetAxis("V2");
        }




        if (forwards > 0)
        {
            transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);


            speed = speed + acceleration * Time.deltaTime;


        }
        else if (forwards < 0)
        {
            transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);

            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            if (speed > 0)
            {
                speed = Mathf.Max(speed - brake * Time.deltaTime, 0);
            }
            else
            {
                speed = Mathf.Min(speed + brake * Time.deltaTime, 0);
            }
        }

        speed = Mathf.Clamp(speed, -MaxSpeed, MaxSpeed);

        Vector2 velocity = Vector2.up * speed;

        if (forwards != 0)
        {
            transform.Translate(velocity * Time.deltaTime, Space.Self);
        }
	}
    void FixedUpdate()
    {
        Debug.Log("Fixed Time = " +Time.fixedTime);
        Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;

        rigidbody.AddForce(dir.normalized * force);

        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }



        dir = dir.normalized;
        rigidbody.velocity = dir * speed;
        rigidbody.velocity = vel;
        rigidbody.position = pos;

    }


    private Vector3 GetMousePosition()
    {
        // create a ray from the camera 
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

}
